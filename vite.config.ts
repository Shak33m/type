import { defineConfig } from "vite";
import PreactPreset from "@preact/preset-vite";
import AutoImportPlugin from "unplugin-auto-import/vite";
import BabelPlugin from "vite-plugin-babel";

export default () => {
  const isPages = process.env.VITE_GITLAB_PAGES == 'true'
  console.log({isPages})

  return defineConfig({
    base: isPages ? '' : '/type/',
    server: {
      host: '0.0.0.0',
    },
    plugins: [
      BabelPlugin({
        filter: /\.(j|t)sx?/,
        loader: (path) => "jsx",
        babelConfig: {
          babelrc: false,
          configFile: false,
          presets: [
            // "@babel/preset-env",
            ["@babel/preset-typescript", {onlyRemoveTypeImports: true}],
          ],
          plugins: [
            "transform-react-pug",
            "transform-jsx-classname-components",
          ],
        },
      }),
      AutoImportPlugin({
        dts: './src/auto-imports.d.ts',
        eslintrc: { enabled: true },
        include: [/\.(j|t)sx?$/],
        imports: [
          "preact",
          {
            "react-router-dom": [
              "useNavigate",
              "useLocation",
              "useParams",
              "Link",
            ],
            "swr": [["default", "useSWR"], "mutate"],
            "preact": ["render"],
            "preact/compat": [["default", "React"], "Suspense"],
            "react-error-boundary": ["ErrorBoundary"],
            "@fortawesome/fontawesome-svg-core": ["library"],
            "@fortawesome/free-solid-svg-icons": ["fas"],
            "@fortawesome/free-regular-svg-icons": ["far"],
            "@fortawesome/react-fontawesome": [["FontAwesomeIcon", "Icon"]],
          },
        ],
        dirs: [
          "./src",
          "./src/**",
        ],
      }),
      PreactPreset(),
    ],
  });
}
