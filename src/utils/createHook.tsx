export const createHook = (provide) => {
  const Context = React.createContext({})

  const Provider = (props) => {
    const value = provide(props)
    return <Context.Provider value={value}>{props.children}</Context.Provider>
  }

  const Consumer = Context.Consumer

  const useContext = () => React.useContext(Context)

  return {Context, Provider, Consumer, useContext}
}
