export const keys = [
  [
    {
      "up": "`",
      "down": "~",
      "code": "Backquote",
      "finger": "1"
    },
    {
      "up": "1",
      "down": "!",
      "code": "Digit1",
      "finger": "1"
    },
    {
      "up": "2",
      "down": "@",
      "code": "Digit2",
      "finger": "1"
    },
    {
      "up": "3",
      "down": "#",
      "code": "Digit3",
      "finger": "2"
    },
    {
      "up": "4",
      "down": "$",
      "code": "Digit4",
      "finger": "3"
    },
    {
      "up": "5",
      "down": "%",
      "code": "Digit5",
      "finger": "4"
    },
    {
      "up": "6",
      "down": "^",
      "code": "Digit6",
      "finger": "4"
    },
    {
      "up": "7",
      "down": "&",
      "code": "Digit7",
      "finger": "5"
    },
    {
      "up": "8",
      "down": "*",
      "code": "Digit8",
      "finger": "6"
    },
    {
      "up": "9",
      "down": "(",
      "code": "Digit9",
      "finger": "7"
    },
    {
      "up": "0",
      "down": ")",
      "code": "Digit0",
      "finger": "8"
    },
    {
      "up": "-",
      "down": "_",
      "code": "Minus",
      "finger": "8"
    },
    {
      "up": "=",
      "down": "+",
      "code": "Equal",
      "finger": "8"
    },
    {
      "up": "delete",
      "down": "delete",
      "code": "Backspace",
      "finger": "8"
    }
  ],
  [
    {
      "up": "tab",
      "down": "tab",
      "code": "Tab",
      "finger": "1"
    },
    {
      "up": "q",
      "down": "Q",
      "code": "KeyQ",
      "finger": "1"
    },
    {
      "up": "w",
      "down": "W",
      "code": "KeyW",
      "finger": "2"
    },
    {
      "up": "e",
      "down": "E",
      "code": "KeyE",
      "finger": "3"
    },
    {
      "up": "r",
      "down": "R",
      "code": "KeyR",
      "finger": "4"
    },
    {
      "up": "t",
      "down": "T",
      "code": "KeyT",
      "finger": "4"
    },
    {
      "up": "y",
      "down": "Y",
      "code": "KeyY",
      "finger": "5"
    },
    {
      "up": "u",
      "down": "U",
      "code": "KeyU",
      "finger": "5"
    },
    {
      "up": "i",
      "down": "I",
      "code": "KeyI",
      "finger": "6"
    },
    {
      "up": "o",
      "down": "O",
      "code": "KeyO",
      "finger": "7"
    },
    {
      "up": "p",
      "down": "P",
      "code": "KeyP",
      "finger": "8"
    },
    {
      "up": "[",
      "down": "{",
      "code": "BracketLeft",
      "finger": "8"
    },
    {
      "up": "]",
      "down": "}",
      "code": "BracketRight",
      "finger": "8"
    },
    {
      "up": "\\",
      "down": "|",
      "code": "Backslash",
      "finger": "8"
    }
  ],
  [
    {
      "up": "caps",
      "down": "caps",
      "code": "CapsLock",
      "finger": "1"
    },
    {
      "up": "a",
      "down": "A",
      "code": "KeyA",
      "finger": "1"
    },
    {
      "up": "s",
      "down": "S",
      "code": "KeyS",
      "finger": "2"
    },
    {
      "up": "d",
      "down": "D",
      "code": "KeyD",
      "finger": "3"
    },
    {
      "up": "f",
      "down": "F",
      "code": "KeyF",
      "finger": "4"
    },
    {
      "up": "g",
      "down": "G",
      "code": "KeyG",
      "finger": "4"
    },
    {
      "up": "h",
      "down": "H",
      "code": "KeyH",
      "finger": "5"
    },
    {
      "up": "j",
      "down": "J",
      "code": "KeyJ",
      "finger": "5"
    },
    {
      "up": "k",
      "down": "K",
      "code": "KeyK",
      "finger": "6"
    },
    {
      "up": "l",
      "down": "L",
      "code": "KeyL",
      "finger": "7"
    },
    {
      "up": ";",
      "down": ":",
      "code": "Semicolon",
      "finger": "8"
    },
    {
      "up": "'",
      "down": "\"",
      "code": "Quote",
      "finger": "8"
    },
    {
      "up": "enter",
      "down": "enter",
      "code": "Enter",
      "finger": "8"
    }
  ],
  [
    {
      "up": "shift",
      "down": "shift",
      "code": "ShiftLeft",
      "finger": "1"
    },
    {
      "up": "z",
      "down": "Z",
      "code": "KeyZ",
      "finger": "1"
    },
    {
      "up": "x",
      "down": "X",
      "code": "KeyX",
      "finger": "2"
    },
    {
      "up": "c",
      "down": "C",
      "code": "KeyC",
      "finger": "3"
    },
    {
      "up": "v",
      "down": "V",
      "code": "KeyV",
      "finger": "4"
    },
    {
      "up": "b",
      "down": "B",
      "code": "KeyB",
      "finger": "4"
    },
    {
      "up": "n",
      "down": "N",
      "code": "KeyN",
      "finger": "5"
    },
    {
      "up": "m",
      "down": "M",
      "code": "KeyM",
      "finger": "5"
    },
    {
      "up": ",",
      "down": "<",
      "code": "Comma",
      "finger": "6"
    },
    {
      "up": ".",
      "down": ">",
      "code": "Period",
      "finger": "7"
    },
    {
      "up": "/",
      "down": "?",
      "code": "Slash",
      "finger": "8"
    },
    {
      "up": "shift",
      "down": "shift",
      "code": "ShiftRight",
      "finger": "8"
    }
  ],
  [
    {
      "up": "space",
      "down": "space",
      "code": "Space",
      "finger": "0"
    }
  ]
]