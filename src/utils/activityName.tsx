export const activityName = ({position, lessonPosition}) => {
  const letter = 'abcdefghijklmnopqrstuvwxyz'[position]
  return `${lessonPosition + 1}${letter}`
}
