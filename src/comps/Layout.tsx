const BUTTON_CLASSES =
  'px-3 text-blue-800 dark_text-blue-200 hover_text-blue-600 dark_hover_text-blue-400'

const $leftLink = (title, icon, to) => pug`
  Link(to=to)
    button(className=BUTTON_CLASSES)
      Icon(icon=icon)
      span.pl-3= title
`

const DefaultTitle = () => pug`
  span Typing
`

export const Layout = ({left='test', Title=DefaultTitle, right=true, children}) => pug`
  - const {muted, setMuted} = useSounds()

  .w-full.h-screen.flex.flex-col.bg-blue-200.dark_bg-gray-800
    .w-full.h-12.grid.grid-cols-12.border-b.border-gray-500.bg-gray-100.dark_bg-gray-900.shadow
      .col-span-3.flex.items-center
        case left
          when 'test'
            = $leftLink('Ninja Mode', 'trophy', '/test')
          when 'back'
            = $leftLink('Activities', 'keyboard', '/lessons')

      .col-span-6.flex.items-center.justify-center.text-3xl.font-medium.text-gray-900.dark_text-gray-100
        React.Suspense(fallback=${pug``}): Title

      .col-span-3.flex.items-center.justify-end.pr-2
        if right
          button.w-12.flex.items-center(className=BUTTON_CLASSES onClick=()=>setMuted(!muted))
            Icon.text-xl(icon=(muted ? 'volume-off' : 'volume-up'))

    .flex-1.h-full.w-full.overflow-y-scroll
      Suspend= children
`
