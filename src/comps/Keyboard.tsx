export const Keyboard = ({shift, keysDown, capsLock}) => {
  return pug`
    .p-2.justify-between.text-gray-800.dark_text-gray-200.border.border-gray-600.dark_border-gray-400.rounded-lg.shadow-lg(
      style={height: 220, width: 662}
    )
      for row, i in keys
        for key, j in row
          .float-left.flex.items-center.justify-center.border.rounded.shadow(
            style=keyStyle(key, j == row.length - 1)
            className=keyClasses(key.finger, keysDown[key.code] || (capsLock && key.up === 'caps'), key.up === 'caps')
            key='key-'+i+'-'+j
            ...key
          )
            span= shift ? key.down : key.up
  `
}

const keyStyle = (key, isLastRow = false) => {
  const style = {
    height: 37,
    width: keyWidth(key),
    marginLeft: 0,
    marginRight: 4,
    marginBottom: 4,
    marginTop: 0,
    clear: null,
  }

  if (key.up === 'caps' || key.up === 'tab' || key.code === 'ShiftLeft') style.clear = 'left'
  if (key.up === 'space') style.marginLeft = 140
  if (isLastRow) style.marginRight = 0

  return style
}

const keyWidth = (key) => KEY_WIDTHS[key.code] || 40

const KEY_WIDTHS = {
  Tab: 68,
  Backspace: 68,
  CapsLock: 78,
  Enter: 75,
  ShiftLeft: 93,
  ShiftRight: 105,
  Space: 350,
}

const keyClasses = (finger, isDown, isCaps) => {
  if (isCaps) {
    return isDown
      ? 'bg-red-400 dark_bg-red-600 border-red-500 animate animate-bounce'
      : 'bg-gray-300 dark_bg-gray-500 border-gray-600 dark_border-gray-400'
  }

  switch (finger) {
    case '0':
      return isDown
        ? 'bg-gray-400 dark_bg-gray-600 border-gray-500'
        : 'bg-gray-300 dark_bg-gray-500 border-gray-600 dark_border-gray-400'

    case '1':
      return isDown
        ? 'bg-blue-400 dark_bg-blue-600 border-blue-500 dark_bg-opacity-80'
        : 'bg-blue-300 dark_bg-blue-500 border-blue-600 dark_border-blue-400 dark_bg-opacity-50'

    case '2':
      return isDown
        ? 'bg-purple-400 dark_bg-purple-600 border-purple-500 dark_bg-opacity-80'
        : 'bg-purple-300 dark_bg-purple-500 border-purple-600 dark_border-purple-400 dark_bg-opacity-50'

    case '3':
      return isDown
        ? 'bg-green-400 dark_bg-green-600 border-green-500 dark_bg-opacity-80'
        : 'bg-green-300 dark_bg-green-500 border-green-600 dark_border-green-400 dark_bg-opacity-50'

    case '4':
      return isDown
        ? 'bg-yellow-400 dark_bg-yellow-600 border-yellow-500 dark_bg-opacity-80'
        : 'bg-yellow-300 dark_bg-yellow-500 border-yellow-600 dark_border-yellow-400 dark_bg-opacity-50'

    case '5':
      return isDown
        ? 'bg-red-400 dark_bg-red-600 border-red-500 dark_bg-opacity-80'
        : 'bg-red-300 dark_bg-red-500 border-red-600 dark_border-red-400 dark_bg-opacity-50'

    case '6':
      return isDown
        ? 'bg-green-400 dark_bg-green-600 border-green-500 dark_bg-opacity-80'
        : 'bg-green-300 dark_bg-green-500 border-green-600 dark_border-green-400 dark_bg-opacity-50'

    case '7':
      return isDown
        ? 'bg-purple-400 dark_bg-purple-600 border-purple-500 dark_bg-opacity-80'
        : 'bg-purple-300 dark_bg-purple-500 border-purple-600 dark_border-purple-400 dark_bg-opacity-50'

    case '8':
      return isDown
        ? 'bg-blue-400 dark_bg-blue-600 border-blue-500 dark_bg-opacity-80'
        : 'bg-blue-300 dark_bg-blue-500 border-blue-600 dark_border-blue-400 dark_bg-opacity-50'

    default:
      return isDown
        ? 'bg-gray-400 dark_bg-gray-600 border-gray-500 dark_bg-opacity-80'
        : 'bg-gray-300 dark_bg-gray-500 border-gray-600 dark_border-gray-400 dark_bg-opacity-50'
  }
}
