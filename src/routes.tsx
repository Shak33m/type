import {createHashRouter, RouterProvider, Routes as Switch, Route, Navigate} from 'react-router-dom'

const route = (path, Element) => ({path, element: <Element/>})
const redirect = (to) => () => <Navigate to={to}/>

const router = createHashRouter([
  route('/lessons', LessonsPage),
  route('/exercises/:id', ExercisePage),
  route('/exercise-results', ExerciseResultsPage),
  route('/test', TestPage),
  route('/test-results', TestResultsPage),
  route('*', redirect('/lessons')),
])

export const Routes = () => pug`
  RouterProvider(router=router)
`
