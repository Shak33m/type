import {BrowserRouter} from 'react-router-dom'

import './styles.css'

// https://fontawesome.com/docs/web/use-with/react/add-icons#add-icons-globally
library.add(fas, far)

render(pug`
  if isMobile()
    .w-screen.h-screen.p-4.flex.flex-col.justify-center.gap-4.bg-blue-200
      .flex.flex-row.gap-4
        .text-6xl 👋
        .text-xl.text-gray-800 You need a real keyboard to practice touch-typing.
      .text-lg.w-full.text-right.text-blue-800.italic ~ Your friends at #[a.underline(href='https://techlitafrica.org') TechLit]

  else
    SoundsProvider
      Routes
`, document.getElementById('root') as HTMLElement)
