export default ({onKeyDownRef, onKeyUpRef}: {
  onKeyDownRef?: any,
  onKeyUpRef?: any,
} = {}) => {
  const [shift, setShift] = useState(false)
  const [capsLock, setCapsLock] = useState(false)

  const [keysDown, setKeyDown] = useReducer((keys, newKeys: object) => ({...keys, ...newKeys}), {})
  useEffect(() => {
    const syncKeyFn = (isDown) => (event) => {
      isDown ? onKeyDownRef?.current?.(event) : onKeyUpRef?.current?.(event)

      setShift(event.shiftKey)
      setCapsLock(event.getModifierState('CapsLock'))
      if (event.code != 'CapsLock') setKeyDown({[event.code]: isDown})
      console.log({capsLock, code: event.code, capsMod: event.getModifierState('CapsLock')})
    }

    const _onKeyDown = syncKeyFn(true)
    const _onKeyUp = syncKeyFn(false)

    window.addEventListener('keydown', _onKeyDown)
    window.addEventListener('keyup', _onKeyUp)

    return () => {
      window.removeEventListener('keydown', _onKeyDown)
      window.removeEventListener('keyup', _onKeyUp)
    }
  }, [])

  return [shift, capsLock, keysDown]
}
