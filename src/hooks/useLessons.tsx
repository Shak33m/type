const isPages = import.meta.env.VITE_GITLAB_PAGES == 'true'
console.log({isPages})

export const getLessons = async () => {
  const response = await fetch(`${isPages ? '' : '/type'}/lessons.json`, {
    headers: {'Authentication': `Basic ${btoa('admin:empowerwatoto')}`},
  })
  return await response.json()
}

export const useLessons = () => {
  const {data, error, isLoading} = useSWR([true], getLessons, {suspense: true})
  const lessons = data?.lessons ?? []

  return React.useMemo(() => {
    let prevLessonId = null
    let prevExerciseId = null

    const lessonsById = {}
    const exercisesById = {}

    lessons.forEach(({exercises, ...lesson}, lessonPosition) => {
      lessonsById[lesson.id] = {
        ...lesson,
        position: lessonPosition,
        exerciseIds: exercises.map(({id}) => id),
        prevLessonId,
        nextLessonId: null,
      }
      if (prevLessonId) lessonsById[prevLessonId].nextLessonId = lesson.id
      prevLessonId = lesson.id

      exercises.forEach((exercise, position) => {
        exercisesById[exercise.id] = {
          ...exercise,
          position,
          prevExerciseId,
          nextExerciseId: null,
          lessonId: lesson.id,
          lessonTitle: lesson.title,
          lessonPosition: lessonPosition,
          lessonExerciseCount: lessonsById[lesson.id].exerciseIds.length
        }
        if (prevExerciseId) exercisesById[prevExerciseId].nextExerciseId = exercise.id
        prevExerciseId = exercise.id
      })
    })

    const lessonsView = lessons.map(({id}) => {
      const lesson = lessonsById[id]
      return {
        ...lesson,
        exercises: lesson.exerciseIds.map((id) => exercisesById[id]),
      }
    })

    return {lessons: lessonsView, lessonsById, exercisesById, error, isLoading}
  }, [data?.lessons])
}
