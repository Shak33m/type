const rand = () => Math.floor(Math.random() * 3) + 1

const createSoundPool = (url) => {
  const sounds = [1, 2, 3].map(() => {
    const sound = new window.Audio(url)
    sound.load()
    return sound
  })

  return {sounds}
}

const playSoundFromPool = (muted, pool) => {
  if (muted) return

  let sound = pool.sounds.find((s) => s.paused)
  if (!sound) {
    sound = pool.sounds[0]
    sound.pause()
    sound.currentTime = 0
  }
  sound.play()
}

export type SoundPlayers = {
  playKeyUp?: () => void,
  playKeyDown?: () => void,
  playOops?: () => void,
  playStart?: () => void,
  playEnd?: () => void,
}
export type Sounds = SoundPlayers & {
  muted: boolean,
  setMuted: () => void,
}

const isPages = import.meta.env.VITE_GITLAB_PAGES == 'true'
console.log({isPages})

export const {Provider: SoundsProvider, useContext: useSounds} =
  createHook(() => {
    const [sounds, setSounds] = useState(null)
    const [muted, setMuted] = useState(false)

    useEffect(() => {
      const newSounds =
        [
          'up-1', 'up-2', 'up-3',
          'down-1', 'down-2', 'down-3',
          'start-1', 'start-2', 'start-3',
          'end-1', 'end-2', 'end-3',
          'oops',
        ]
          .reduce((s, name) => {
            s[name] = createSoundPool(`${isPages ? '' : '/type'}/sounds/${name}.mp3`)
            return s
          }, {})

      setSounds(newSounds)
    }, [])

    const soundPlayers = useMemo<SoundPlayers>(() =>
      sounds
        ? {
            playKeyUp: () => playSoundFromPool(muted, sounds[`up-${rand()}`]),
            playKeyDown: () => playSoundFromPool(muted, sounds[`down-${rand()}`]),
            playOops: () => playSoundFromPool(muted, sounds.oops),
            playStart: () => playSoundFromPool(muted, sounds[`start-${rand()}`]),
            playEnd: () => playSoundFromPool(muted, sounds[`end-${rand()}`]),
          }
        : {}, [sounds, muted])

    return {
      muted,
      setMuted,
      ...soundPlayers,
    } as Sounds
  })

export default useSounds
