import { Sounds } from "../hooks/useSounds"

const randomTest = () => {
  if(window.localStorage.getItem('currentChallenge') == null){
    updateCurrentChallenge()
  }
  const data = JSON.parse(window.localStorage.getItem('currentChallenge'))
  return data
}
const updateCurrentChallenge = () => {
  const uniq = {}
  while (Object.keys(uniq).length < 3) {
    uniq[testLines.length * Math.random() | 0] = true
  }

  const data = Object.keys(uniq).map((i) => testLines[i]).join(' ')
  
  window.localStorage.setItem('currentChallenge', JSON.stringify(data))
}
const testInit = {
  wpm: 0,
  typos: 0,
  accuracy: 1,
  wrong: false,
  cursor: 0,
  ready: false,
  startedAt: null,
  stoppedAt: null,
}

export const TestPage = () => {
  const navigate = useNavigate()

  const [{
    content,
    wpm,
    typos,
    accuracy,
    wrong,
    cursor,
    ready,
    startedAt,
    stoppedAt,
  }, reducer] =
    useReducer(
      (state, {type, data}: {type: string, data?: any}) => ({
        update: (newState) => ({...state, ...newState}),
        wpmTick: () => {
          if (!state.startedAt || state.stoppedAt) return state
          const words = (state.cursor + 1) / 5
          const minutes = (+new Date() - state.startedAt) / 1000 / 60
          return {
            ...state,
            wpm: Math.round(words / minutes),
            accuracy: (state.cursor - state.typos) / state.cursor,
          }
        },
      })[type]?.(data) || state,
      {
        ...testInit,
        content: randomTest(),
      },
    )

  const update = (s2) => reducer({type: 'update', data: s2})

  const {
    playOops,
    playKeyUp,
    playKeyDown,
    playStart,
    playEnd,
  } = useSounds() as Sounds

  const restart = () => {
    playStart?.()
    update(testInit)

    // TODO: add a UI effect that makes a 1000ms timeout less surprising
    setTimeout(() => update({ready: true}), 10)
  }

  useEffect(() => {
    restart()
    const wpmTick = setInterval(() => reducer({type: 'wpmTick'}), 300)
    return () => clearInterval(wpmTick)
  }, [])

  const keyDownRef = useRef<(event: any)=>void>()
  const keyUpRef = useRef<(event: any)=>void>()

  keyDownRef.current = (event) => {
    if (!ready || event.ctrlKey || event.altKey) return

    if (
      event.code.startsWith('Key') ||
      event.code.startsWith('Digit') ||
      [
        'Backquote',
        'Space',
        'Backspace',
        'Backslash',
        'BracketRight',
        'BracketLeft',
        'Quote',
        'Semicolon',
        'Slash',
        'Period',
        'Comma',
      ].includes(event.code)
    ) {
      event.stopPropagation()
      event.preventDefault()
    }

    playKeyDown()

    if (event.code === 'Escape') {
      event.preventDefault()
      return restart()
    }

    if (stoppedAt) return

    if (event.key === content[cursor]) {
      update({
        cursor: cursor + 1,
        wrong: false,
      })

      if (!startedAt) update({startedAt: new Date()})

      if (cursor >= content.length - 1) {
        update({stoppedAt: new Date()})

        playEnd()

        const accuracy = (content.length - typos) / content.length
        const state = {content, typos, wpm, accuracy}
        updateCurrentChallenge();
        navigate('/test-results', {state})
      }
      return
    }

    if (
      event.ctrlKey ||
      event.altKey ||
      ['ShiftLeft', 'ShiftRight', 'CapsLock'].includes(event.code)
    ) return

    update({wrong: true, typos: typos + 1})
  }

  keyUpRef.current = (event) => {
    playKeyUp()
  }

  const [shift, capsLock, keysDown] = useKeysDown({
    onKeyDownRef: keyDownRef,
    onKeyUpRef: keyUpRef,
  })

  return pug`
    Layout(left='back' Title=()=>'Ninja Mode')
      .w-full.flex-1.flex.flex-col.items-center.justify-start
        .w-full.max-w-screen-lg.mx-auto.h-24.my-8.flex.justify-between
          .h-full.flex.items-center
            .flex.flex-col.items-center.px-8
              .text-4xl.text-gray-900.dark_text-gray-100= wpm
              .text-lg.text-gray-500.font-medium wpm
            .flex.flex-col.items-center.px-8
              .flex.items-end.text-gray-900.dark_text-gray-100
                .text-4xl= Math.round(accuracy * 100)
                .pl-1.text-2xl %
              .text-lg.text-gray-500.font-medium accuracy

        .w-full.p-2.text-2xl.tracking-wide.bg-white.dark_bg-black.rounded.border.border-gray-500.shadow-xl(
          className=(ready ? 'text-gray-900 dark_text-gray-100' : 'text-gray-600 dark_text-gray-400')
          style={maxWidth: '900px'}
        )
          for letter, position in content.split('')
            Letter(
              key=position
              letter=letter
              ready=ready
              cursor=cursor
              position=position
              wrong=wrong
            )
  `
}

const Letter = ({letter, ready, position, cursor, wrong}) => pug`
  span(
    className=letterClassName({ready, position, cursor, wrong})
    style={
      marginLeft: '-.05em',
      marginRight: '-.05em',
      paddingLeft: '.05em',
      paddingRight: '.05em',
      borderRadius: '.075em',
    }
  )= letter
`

const letterClassName = ({ready, position, cursor, wrong}) => {
  if (!ready) return 'text-gray-600 dark_text-gray-400'
  if (cursor == position) {
    if (wrong) return 'bg-red-300 dark_bg-red-700'
    return 'bg-green-300 dark_bg-green-700'
  }
  if (cursor < position) return 'text-black dark_text-white'
  return 'text-gray-600 dark_text-gray-400'
}
