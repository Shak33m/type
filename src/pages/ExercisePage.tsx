import { activityName } from "../utils/activityName"
import { Sounds } from "../hooks/useSounds"

export default () => pug`
  Layout(left='back' Title=ReadyTitle): Ready
`

const useExercise = () => {
  const {id} = useParams()
  const {exercisesById} = useLessons()
  return exercisesById[id]
}

const ReadyTitle = () => pug`
  - const exercise = useExercise()
  span.pr-4.text-2xl.text-gray-800.font-semibold.truncate #[small.opacity-70 #{exercise.lessonTitle} (#{exercise.position + 1}/#{exercise.lessonExerciseCount}):] #{activityName(exercise)}. #{exercise.title}
`

const exerciseInit = {
  wpm: 0,
  typos: 0,
  wrong: false,
  cursor: 0,
  ready: false,
  startedAt: null,
  stoppedAt: null,
}

const Letter = ({letter, ready, position, cursor, wrong}) => pug`
  span(
    className=letterClassName({ready, position, cursor, wrong})
    style={
      marginLeft: '-.05em',
      marginRight: '-.05em',
      paddingLeft: '.05em',
      paddingRight: '.05em',
      borderRadius: '.075em',
    }
  )= letter
`

const letterClassName = ({ready, position, cursor, wrong}) => {
  if (!ready) return 'text-gray-600 dark_text-gray-400'
  if (cursor == position) {
    //if (wrong) return 'bg-red-300 dark_bg-red-700'
    return 'bg-green-300 dark_bg-green-700'
  }
  if (cursor < position) return 'text-black dark_text-white'
  return 'text-gray-600 dark_text-gray-400'
}

const Ready = () => {
  const {id} = useParams()
  const navigate = useNavigate()
  const exercise = useExercise()

  const [{
    wpm,
    typos,
    wrong,
    cursor,
    ready,
    startedAt,
    stoppedAt,
  }, reducer] =
    React.useReducer(
      (state, {type, data}: {type: string, data?: any}) => ({
        update: (newState) => ({...state, ...newState}),
        wpmTick: () => {
          if (!state.startedAt || state.stoppedAt) return state
          const words = (state.cursor + 1) / 5
          const minutes = (+new Date() - state.startedAt) / 1000 / 60
          return {
            ...state,
            wpm: Math.round(words / minutes),
          }
        },
      })[type]?.(data) || state,
      {...exerciseInit},
    )

  const update = (s2) => reducer({type: 'update', data: s2})

  const {
    playOops,
    playKeyUp,
    playKeyDown,
    playStart,
    playEnd,
  } = useSounds() as Sounds

  const restart = () => {
    playStart()
    update(exerciseInit)

    // TODO: add a UI effect that makes a 1000ms timeout less surprising
    setTimeout(() => update({ready: true}), 10)
  }

  React.useEffect(() => {
    restart()
    const wpmTick = setInterval(() => reducer({type: 'wpmTick'}), 300)
    return () => clearInterval(wpmTick)
  }, [])

  const keyDownRef = React.useRef<(event: any)=>void>()
  const keyUpRef = React.useRef<(event: any)=>void>()

  keyDownRef.current = (event) => {
    if (!ready || event.ctrlKey || event.altKey) return

    if (
      event.code.startsWith('Key') ||
      event.code.startsWith('Digit') ||
      [
        'Backquote',
        'Space',
        'Backspace',
        'Backslash',
        'BracketRight',
        'BracketLeft',
        'Quote',
        'Semicolon',
        'Slash',
        'Period',
        'Comma',
      ].includes(event.code)
    ) {
      event.stopPropagation()
      event.preventDefault()
    }

    playKeyDown()

    if (event.code === 'Escape') {
      event.preventDefault()
      return restart()
    }

    if (stoppedAt) return

    if (event.key === exercise.content[cursor] && !event.getModifierState('CapsLock')) {
      update({
        cursor: cursor + 1,
        wrong: false,
      })

      if (!startedAt) update({startedAt: new Date()})

      if (cursor >= exercise.content.length - 1) {
        update({stoppedAt: new Date()})

        playEnd()

        const length = exercise.content.length
        const accuracy = (length - typos) / length

        const state = {id, typos, wpm, accuracy}
        navigate('/exercise-results', {state})
      }
      return
    }

    if (
      event.ctrlKey ||
      event.altKey ||
      ['ShiftLeft', 'ShiftRight', 'CapsLock'].includes(event.code)
    ) return

    update({wrong: true, typos: typos + 1})

    if (event.getModifierState('CapsLock')) playOops()
  }

  keyUpRef.current = (event) => {
    playKeyUp()
  }

  const [shift, capsLock, keysDown] = useKeysDown({
    onKeyDownRef: keyDownRef,
    onKeyUpRef: keyUpRef,
  })

  return pug`
    .flex-1.w-full.mx-auto.px-4.flex.flex-col.justify-center.items-center(style={maxWidth: '960px'})
      .w-full.mt-24.mb-8.flex-1.flex.items-center.justify-center.bg-white.dark_bg-black.rounded.border.border-gray-500.shadow-xl(style={maxWidth: '900px'})
        .p-2.text-4xl.tracking-wide(
          className=(ready ? 'text-gray-900 dark_text-gray-100' : 'text-gray-600 dark_text-gray-400')
        )
          for letter, position in exercise.content.split('')
            Letter(
              key=position
              letter=letter
              ready=ready
              cursor=cursor
              position=position
              wrong=wrong
            )

      Keyboard(shift=shift keysDown=keysDown capsLock=capsLock)
  `
}
