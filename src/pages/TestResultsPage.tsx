import { useInterval } from "../hooks/useInterval"

export const TestResultsPage = () => pug`
  Layout: Ready
`

const Ready = () => {
  const navigate = useNavigate()
  const navigation = useLocation()
  const [seconds, setSeconds] = React.useState(10)

  const keyDownRef = useRef<(event: any)=>void>()
  keyDownRef.current = (event) => {
    if (event.code === 'Escape') {
      navigate('/test')
    }
    if (event.code === 'Enter') {
      navigate('/test')
    }
  }

  useKeysDown({
    onKeyDownRef: keyDownRef,
  })

  useInterval(() => { setSeconds((s) => s - 1) }, 1000)
  React.useEffect(() => { seconds < 1 && navigate('/test') }, [seconds])

  return pug`
    .w-full.flex-1.flex.flex-col.overflow-y-scroll.items-center.p-8
      .w-full.max-w-screen-md.flex.flex-col.items-center.py-4.border.border-gray-500.bg-gray-100.dark_bg-gray-900.shadow.rounded

        .w-full.text-center.pt-8.pb-2.text-4xl.text-gray-900.dark_text-gray-100 Test Results

        .w-full.flex.justify-between
          Link(to='/test')
            button.w-48.flex.flex-col.items-center.text-blue-800.dark_text-blue-200.hover_text-blue-700.dark_hover_text-blue-300.active_text-blue-800.dark_active_text-blue-200.font-medium
              .w-9.h-9.flex.items-center.justify-center.text-2xl.text-white.font-black.rounded-full.bg-blue-800= seconds
              span.text-2xl Try again
              span.text-gray-500 (Escape)

          .flex.items-center.justify-center.pt-8.pb-12
            Icon.text-yellow-500(icon='award' style={fontSize: '12em'})

          Link(to='/test')
            button.w-48.flex.flex-col.items-center.text-blue-800.dark_text-blue-200.hover_text-blue-700.dark_hover_text-blue-300.active_text-blue-800.dark_active_text-blue-200.font-medium
              Icon.text-4xl(icon='arrow-alt-circle-right')
              span.text-2xl Continue
              span.text-gray-500 (Enter)

        .flex.items-end.justify-between(style={width: '20em'})
          .flex.flex-col.items-center
            .text-6xl.text-gray-900.dark_text-gray-100= navigation.state.wpm
            .text-2xl.text-gray-500.font-medium wpm
          .flex.flex-col.items-center
            .text-6xl.text-gray-900.dark_text-gray-100 #{Math.round(navigation.state.accuracy * 100)}%
            .text-2xl.text-gray-500.font-medium accuracy
  `
}
