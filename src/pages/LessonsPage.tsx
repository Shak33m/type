import { activityName } from "../utils/activityName"

export const LessonsPage = () => pug`
  Layout
    .w-full.flex-1.flex.flex-col.items-center.justify-center.p-8
      .w-full.max-w-screen-sm.p-4.border.border-gray-500.bg-gray-100.dark_bg-gray-900.shadow.rounded
        Ready
`

const Ready = () => pug`
  - const {lessons} = useLessons()
  - const [openId, setOpenId] = useState(() => '')

  for lesson in lessons
    - const isOpen = lesson.id === openId
    .pt-2.pb-4.mb-2.flex.flex-col.border-b.border-gray-300.dark_border-gray-700(key=lesson.id id='accordion-'+lesson.id)
      button.flex-1.flex.flex-row.items-center.justify-between(
        onClick=()=>setOpenId(isOpen ? '' : lesson.id)
      )
        .text-xl.text-gray-600.dark_text-gray-400 #{lesson.position + 1}. #{lesson.title}

        .text-blue-600.dark_text-blue-400.hover_text-blue-400.dark_hover_text-blue-600
          Icon.text-2xl(icon=isOpen ? 'chevron-up' : 'chevron-down')

      if isOpen
        .grid.grid-cols-3.gap-4.py-4
          for exercise in lesson.exercises
            Link(key=exercise.id to=('/exercises/'+exercise.id))
              .h-full.p-2.mt-2.border.border-gray-300.dark_border-gray-800.shadow.rounded.text-center(
                className=exerciseClassName
              )
                .text-xl #{activityName(exercise)}. #{exercise.title}
                .text-xs.text-gray-400.dark_text-gray-600.truncate #{exercise.content}
`

const exerciseClassName = (
  ' bg-gray-50 dark_bg-black text-blue-600 dark_text-blue-400 ' +
  ' hover_bg-white dark_hover_border-blue-800 active_bg-gray-100 ' +
  ' dark_active_bg-gray-900 '
)
