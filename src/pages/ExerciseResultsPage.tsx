import { activityName } from "../utils/activityName"
import { useInterval } from "../hooks/useInterval"
import Confetti from 'react-confetti'

export const ExerciseResultsPage = () => pug`
  Layout(left='back' Title=ReadyTitle): Ready
`

const useResults = () => {
  const navigate = useNavigate()
  const results = useLocation().state
  if (results == 'idle') {
    navigate('/lessons')
    return {}
  }

  const {exercisesById, lessonsById} = useLessons()
  const exercise = exercisesById[results.id]
  const lesson = lessonsById[exercise.lessonId]

  results.exercise = exercise
  results.lesson = lesson

  results.title = exercise.title
  results.lessonTitle = exercise.lessonTitle
  results.position = exercise.position
  results.lessonPosition = lesson.position

  results.nextExerciseId = exercise.nextExerciseId
  results.okayWpm = exercise.okayWpm
  results.okayTypos = exercise.okayTypos
  results.okayWpm = exercise.okayWpm
  results.okayTypos = exercise.okayTypos

  results.accurate = results.typos <= exercise.goodTypos
  results.fast = results.wpm >= exercise.goodWpm && results.typos <= exercise.okayTypos
  results.complete = results.typos <= exercise.okayTypos && results.wpm >= exercise.okayWpm
  results.passedLastExercise = results.complete && results.nextExerciseId == null

  return results
}

const ReadyTitle = () => pug`
  - const results = useResults()
  span.pr-4.text-2xl.text-gray-800.font-semibold.truncate #[small.opacity-70 #{results.lessonTitle}:] #{activityName(results)}. #{results.title}
`

const CountDown = ({prevPath}) => {
  const navigate = useNavigate()
  const [seconds, setSeconds] = React.useState(3)

  useInterval(() => { setSeconds((s) => s - 1) }, 1000)
  React.useEffect(() => { seconds < 1 && navigate(prevPath) }, [seconds])

  return seconds
}

const Ready = () => {
  const navigate = useNavigate()
  const results = useResults()

  const nextPath =
    results.nextExerciseId
      ? `/exercises/${results.nextExerciseId}`
      : '/test'
  const prevPath = `/exercises/${results.id}`

  const keyDownRef = React.useRef<(event: any)=>void>()
  keyDownRef.current = (event) => {
    if (event.code === 'Escape') {
      navigate(prevPath)
    }
    if (event.code === 'Enter') {
      navigate(results.complete ? nextPath : prevPath)
    }
  }

  useKeysDown({
    onKeyDownRef: keyDownRef,
  })

  const [title, subtitle] = messages(results)

  return pug`
      if results.passedLastExercise
        Confetti(width=window.innerWidth height=window.innerHeight)
      .w-full.flex-1.flex.flex-col.overflow-y-scroll.items-center.p-8
        .w-full.max-w-screen-md.flex.flex-col.items-center.py-4.border.border-gray-500.bg-gray-100.dark_bg-gray-900.shadow.rounded

          .w-full.flex.justify-between
            Link(to=prevPath)
              button.w-48.flex.flex-col.items-center.text-blue-800.dark_text-blue-200.hover_text-blue-700.dark_hover_text-blue-300.active_text-blue-800.dark_active_text-blue-200.font-medium
                if results.passedLastExercise
                  Icon.text-4xl(icon='arrow-alt-circle-left')
                else
                  .w-9.h-9.flex.items-center.justify-center.text-2xl.text-white.font-black.rounded-full.bg-blue-800
                    CountDown(prevPath=prevPath)
                span.text-2xl Try again
                span.text-gray-500 (Escape)

            .text-center.pt-8
              .text-4xl.pb-2.text-gray-900.dark_text-gray-100= title
              .text-xl.pb-12.text-gray-700.dark_text-gray-300= subtitle

            if results.complete
              Link(to=nextPath)
                button.w-48.flex.flex-col.items-center.text-blue-800.dark_text-blue-200.hover_text-blue-700.dark_hover_text-blue-300.active_text-blue-800.dark_active_text-blue-200.font-medium
                  Icon.text-4xl(icon='arrow-alt-circle-right')
                  span.text-2xl Continue
                  span.text-gray-500 (Enter)
            else
              .w-48

          .flex.items-center.justify-center
            if results.accurate && results.fast
              Icon.text-yellow-500.animate.animate-bounce(style={fontSize: '8em'} icon='star')

            else
              .px-8
                Icon(
                  style={fontSize: '8em'}
                  icon='check'
                  className=(results.complete ? 'text-yellow-500' : 'text-gray-500')
                )
              .px-8
                Icon(
                  style={fontSize: '8em'}
                  icon='bullseye'
                  className=(results.accurate ? 'text-yellow-500' : 'text-gray-500')
                )
              .px-8
                Icon(
                  style={fontSize: '8em'}
                  icon='bolt'
                  className=(results.fast ? 'text-yellow-500' : 'text-gray-500')
                )

          .py-6.flex.items-end.justify-between(style={width: '18em'})
            .flex.flex-col.items-center
              .text-6xl.text-gray-900.dark_text-gray-100= results.wpm
              .text-2xl.text-gray-500.font-medium wpm
            .flex.flex-col.items-center
              .text-6xl.text-gray-900.dark_text-gray-100= results.typos
              .text-2xl.text-gray-500.font-medium typos
  `
}

const messages = ({
  wpm, typos, complete, accurate, fast,
  okayWpm, okayTypos, goodWpm, goodTypos, passedLastExercise
}) => {
  let title = 'Almost there!'
  if (fast) title = 'That was fast!'
  if (accurate) title = 'That was accurate!'
  if (complete) title = 'You passed!'
  if (passedLastExercise) title = 'You did it 👏🎉'

  let subtitle = ''
  if (typos > okayTypos) {
    if (okayTypos > 0) {
      subtitle = `You need fewer than ${okayTypos + 1} typos to pass.`
    } else {
      subtitle = "You can't make any mistakes on this exercise."
    }
  } else if (wpm < okayWpm) {
    subtitle = `You need to type at least ${okayWpm} words per minute.`
  } else if (typos > goodTypos) {
    if (goodTypos > 0) {
      subtitle = `Can you do it with fewer than ${goodTypos + 1} typos?`
    } else {
      subtitle = 'Can you do it without any typos?'
    }
  } else if (wpm < goodWpm) {
    subtitle = `Can you do it faster than ${goodWpm} words per minute?`
  } else if (wpm >= goodWpm && typos <= goodTypos) {
    title = 'You did it!'
    subtitle = '👏🎉👏🎉👏🎉'
  }

  return [title, subtitle]
}
