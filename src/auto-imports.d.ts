/* eslint-disable */
/* prettier-ignore */
// @ts-nocheck
// noinspection JSUnusedGlobalSymbols
// Generated by unplugin-auto-import
export {}
declare global {
  const BrowserRouter: typeof import('react-router-dom')['BrowserRouter']
  const ErrorBoundary: typeof import('react-error-boundary')['ErrorBoundary']
  const ExercisePage: typeof import('./pages/ExercisePage')['default']
  const ExerciseResultsPage: typeof import('./pages/ExerciseResultsPage')['ExerciseResultsPage']
  const Failed: typeof import('./comps/Suspend')['Failed']
  const HomeLayout: typeof import('./comps/HomeLayout')['HomeLayout']
  const Icon: typeof import('@fortawesome/react-fontawesome')['FontAwesomeIcon']
  const Keyboard: typeof import('./comps/Keyboard')['Keyboard']
  const Layout: typeof import('./comps/Layout')['Layout']
  const LessonsPage: typeof import('./pages/LessonsPage')['LessonsPage']
  const Link: typeof import('react-router-dom')['Link']
  const Loading: typeof import('./comps/Suspend')['Loading']
  const React: typeof import('preact/compat')['default']
  const Redirect: typeof import('react-router-dom')['Redirect']
  const Route: typeof import('react-router-dom')['Route']
  const RouteSwitch: typeof import('react-router-dom')['Switch']
  const Routes: typeof import('./routes')['Routes']
  const SoundsProvider: typeof import('./hooks/useSounds')['SoundsProvider']
  const Suspend: typeof import('./comps/Suspend')['default']
  const Suspense: typeof import('preact/compat')['Suspense']
  const Switch: typeof import('react-router-dom')['Switch']
  const TestPage: typeof import('./pages/TestPage')['TestPage']
  const TestResultsPage: typeof import('./pages/TestResultsPage')['TestResultsPage']
  const TipsPage: typeof import('./pages/TipsPage')['TipsPage']
  const activityName: typeof import('./utils/activityName')['activityName']
  const createHook: typeof import('./utils/createHook')['createHook']
  const disableMobile: typeof import('./utils/disableMobile')['default']
  const far: typeof import('@fortawesome/free-regular-svg-icons')['far']
  const fas: typeof import('@fortawesome/free-solid-svg-icons')['fas']
  const getLessons: typeof import('./hooks/useLessons')['getLessons']
  const isMobile: typeof import('./utils/isMobile.js')['default']
  const keys: typeof import('./utils/keys')['keys']
  const library: typeof import('@fortawesome/fontawesome-svg-core')['library']
  const mutate: typeof import('swr')['mutate']
  const render: typeof import('preact')['render']
  const testLines: typeof import('./utils/testLines')['testLines']
  const tips: typeof import('./utils/tips')['tips']
  const useCallback: typeof import('preact/hooks')['useCallback']
  const useContext: typeof import('preact/hooks')['useContext']
  const useEffect: typeof import('preact/hooks')['useEffect']
  const useInterval: typeof import('./hooks/useInterval.js')['useInterval']
  const useKeysDown: typeof import('./hooks/useKeysDown')['default']
  const useLessons: typeof import('./hooks/useLessons')['useLessons']
  const useLocation: typeof import('react-router-dom')['useLocation']
  const useMemo: typeof import('preact/hooks')['useMemo']
  const useNavigate: typeof import('react-router-dom')['useNavigate']
  const useNavigation: typeof import('react-router-dom')['useNavigation']
  const useParams: typeof import('react-router-dom')['useParams']
  const useReducer: typeof import('preact/hooks')['useReducer']
  const useRef: typeof import('preact/hooks')['useRef']
  const useSWR: typeof import('swr')['default']
  const useSounds: typeof import('./hooks/useSounds')['default']
  const useState: typeof import('preact/hooks')['useState']
  const viteConfig: typeof import('../vite.config')['default']
}
