#### home row position

## left to right
2 20 0 50
asdfjkl;asdfjkl;asdfjkl;asdfjkl;

## right to left
2 20 0 50
;lkjfdsa;lkjfdsa;lkjfdsa;lkjfdsa

## left to right and space
2 20 0 50
asdfjkl; asdfjkl; asdfjkl; asdfjkl;

## right to left and space
2 20 0 50
;lkjfdsa ;lkjfdsa ;lkjfdsa ;lkjfdsa

## left to right more space
2 20 0 50
asdf jkl; asdf jkl; asdf jkl; asdf jkl;

## right to left more space
2 15 0 50
;lkj fdsa ;lkj fdsa ;lkj fdsa ;lkj fdsa

## mirror the fingers
2 20 0 50
a;sldkfja;sldkfja;sldkfja;sldkfj

## mirror the fingers
2 20 0 50
fjdksla;fjdksla;fjdksla;fjdksla;

## left hand
2 15 0 60
asdfasdfasdfasdfasdfasdfasdfasdf

## left hand and thumb
2 15 0 60
asdf asdf asdf asdf asdf asdf asdf asdf

## right hand
2 15 0 50
;lkj;lkj;lkj;lkj;lkj;lkj;lkj;lkj

## right hand and thumb
2 15 0 60
;lkj ;lkj ;lkj ;lkj ;lkj ;lkj ;lkj ;lkj

## both hands
2 15 0 60
asdf ;lkj asdf ;lkj asdf ;lkj asdf ;lkj

## left hand reverse
2 15 0 60
fdsa fdsa fdsa fdsa fdsa fdsa fdsa fdsa

## right hand reverse
2 15 0 60
jkl; jkl; jkl; jkl; jkl; jkl; jkl; jkl;

## both hands reverse
2 15 0 60
fdsa jkl; fdsa jkl; fdsa jkl; fdsa jkl;

## left index finger
2 15 0 60
asdgf asdgf asdgf asdgf asdgf asdgf asdgf asdgf

## right index finger
2 15 0 60
;lkhj ;lkhj ;lkhj ;lkhj ;lkhj ;lkhj ;lkhj ;lkhj

## both index fingers
2 15 0 60
asdgf ;lkhj asdgf ;lkhj asdgf ;lkhj asdgf ;lkhj

#### staying in position

## left hand above
2 15 0 30
awefsd awefsd awefsd awefsd awefsd awefsd awefsd awefsd

## right hand above
2 15 0 30
;oijlk ;oijlk ;oijlk ;oijlk ;oijlk ;oijlk ;oijlk ;oijlk

## both hands above
2 15 0 30
awefsd ;oijlk awefsd ;oijlk awefsd ;oijlk awefsd ;oijlk

## left hand below
2 15 0 30
axcfsd axcfsd axcfsd axcfsd axcfsd axcfsd axcfsd axcfsd

## right hand below
2 15 0 30
;.,jlk ;.,jlk ;.,jlk ;.,jlk ;.,jlk ;.,jlk ;.,jlk ;.,jlk

## left hand above and below
2 15 0 30
awefsd axcfsd awefsd axcfsd awefsd axcfsd awefsd axcfsd

## left hand around
2 15 0 30
awef fcxa awef fcxa awef fcxa awef fcxa

## left hand around
2 15 0 30
awefcxa awefcxa awefcxa awefcxa

## right hand above and below
2 15 0 30
;oij ;.,j ;oij ;.,j ;oij

## right hand around
2 15 0 30
;oij j,.; ;oij j,.; ;oij

## right hand around
2 15 0 30
;oij,.; ;oij,.; ;oij,.; ;oij,.;


## both hands around
2 15 0 30
awefcxa ;oij,.; awefcxa ;oij,.; awefcxa ;oij,.; awefcxa ;oij,.;

## left hand around reversed
2 15 0 30
axcfewa axcfewa axcfewa axcfewa

## right hand around reversed
2 15 0 30
;.,jio; ;.,jio; ;.,jio; ;.,jio;

## both hands around reversed
2 15 0 30
axcfewa ;.,jio; axcfewa ;.,jio; axcfewa ;.,jio; axcfewa ;.,jio;

#### pinkies

## left high
2 15 0 30
qsdr qsdr qsdr qsdr qsdr qsdr qsdr qsdr

## left low
2 15 0 30
zsdv zsdv zsdv zsdv zsdv zsdv zsdv zsdv

## left study
2 15 0 30
qsdf asdf zsdf qsdf asdf zsdf qsdf asdf zsdf

## right high
2 15 0 30
plku plku plku plku plku plku plku plku

## right low
2 15 0 30
/lkm /lkm /lkm /lkm /lkm /lkm /lkm /lkm

## right study
2 15 0 30
plkj ;lkj /lkj plkj ;lkj /lkj plkj ;lkj /lkj

## both high
2 15 0 30
qsdr plku qsdr plku qsdr plku qsdr plku

## both low
2 15 0 30
zsdv /lkm zsdv /lkm zsdv /lkm zsdv /lkm

#### index fingers

## left high
2 15 0 30
asdt asdt asdt asdt asdt asdt asdt asdt

## left mid
2 15 0 30
asdg asdg asdg asdg asdg asdg asdg asdg

## left low
2 15 0 30
asdb asdb asdb asdb asdb asdb asdb asdb

## left study
2 15 0 30
asdt asdg asdb asdt asdg asdb asdt asdg asdb

## right high
2 15 0 30
;lky ;lky ;lky ;lky ;lky ;lky ;lky ;lky

## right mid
2 15 0 30
;lkh ;lkh ;lkh ;lkh ;lkh ;lkh ;lkh ;lkh

## right low
2 15 0 30
;lkn ;lkn ;lkn ;lkn ;lkn ;lkn ;lkn ;lkn

## right study
2 15 0 30
;lky ;lkh ;lkn ;lky ;lkh ;lkn ;lky ;lkh ;lkn

## both high
2 15 0 30
asdt ;lky asdt ;lky asdt ;lky asdt ;lky

## both middle
2 15 0 30
asdg ;lkh asdg ;lkh asdg ;lkh asdg ;lkh

## both low
2 15 0 30
asdb ;lkn asdb ;lkn asdb ;lkn asdb ;lkn

#### home row position

## d and f
2 15 0 30
asdfd fadsf dsafd sfdaf fdfdf dafsd ddaaf ffssd adsfd sadff dffdf fdfdd

## j and k
2 15 0 30
jkl;k ;jklj j;kjk ;k;jk lkljk kjl;k jkljk kj;;k kjllj jjlkj


## j, k, d and f
2 15 0 27
dkfdj jdfkf kjdkf fkjdk fkdkj fkfdj kjdkf dfkfj kfkdj fjkjd

## l and ;
2 15 0 30
;lkj ;;jkl j;kl klj;l ;;kjl jkl;; ;jlk ;lkl; jkl;l lkjl; l;;l;


## First review
2 15 0 30
lfk ;fk djlf kflf ;fkj kj;f dj;;f ;f;lf ;lfkf dljdf ;f;lfkf

## a and s
2 15 0 30
sasad fadsa afsad ssadf aasfd asadf sasas sasad fsasa aasas

## Second review
2 15 0 30
all add; as ask; sad; fas lad dak; dad fad fall; lass dall;

## Home challenge
2 15 0 30
alas dald fall; dad flak; lass sad; fass; all fall lad; ask


#### index fingers 1

## asdf and g
2 15 0 30
asdfg fasgd gadfd ggafd asgfd adfgs asdgf fgfga dsfga gagsg fgasg

## ;lkj and h
2 15 0 30
;lkjh hjl;k hl;hj h;klh hjkl; ;lkhj hhjkh ;lhkh hkhk; h;hlhj

## review 1
2 15 0 30
gad has aha; had flag gas; sag ash; gag dash glag half;

## review 2
2 15 0 30
gaff; hall hald saga hash; sash gall flag; has dash half

## t and back
2 15 0 30
ftast ttfst adtfd ftftf ttasf tatft tstdt tfdtf ftdtf sdtft

## review 3
2 15 0 30
tft tsk alt gst has hat kat sha ska tad tag that tas tsk

## r and back
2 15 0 30
frasr rrfsr adrfd frfrf rrasf rarfr rsrdr rfdrf frdrf sdrfr

## Review with r
2 15 0 30
rag ark rash jarl trash; rash rad lard; drag lar jar

## Left pointer
2 15 0 30
raft; draft arts gats trad task shard hard; hath halt gast

## review 4
2 15 0 30
art tad gar at sat rag tag; far jar tar rah hat rat rag tat

## review 5
2 15 0 30
daft dart jars task; hard tart start grad data talk; shaft rash

## review 6
2 15 0 30
hart; haft karat halt salt dark; raft draft shark; grass

## review 7
2 15 0 30
graft fast raja shark gard shard start lard; flat

## From y to j
2 15 0 30
jy;ly yyjly ;kyjk jyjyj yy;lj y;yjy ylyky yjkyj jykyj lkyjy

## Review with y
2 15 0 30
ya yald yag yak; yard sty fly kays day kyaf slay; yald lys

## From u to j
2 15 0 30
ju;lu uujlu ;kujk jujuj uu;lj u;uju uluku ujkuj jukuj lkuju

## Right pointer
2 15 0 30
uh husk tau sku; ludd yuk ugh uru juku us; guff usla dull

## review 1
2 15 0 30
day fly dug lay jay sky lug fur fry try hut; say lady yard

## review 2
2 15 0 30
lush shut fray surd lurk usual surf flay just lust dust


## Rhytha
2 15 0 30
rhytha rhytha rhytha rhytha rhytha rhytha rhytha rhytha rhytha

## Midterm review
2 15 0 25
yurt lady try tardy stray; stalk ya lathy; flay rhytha


#### index fingers 2

## From v to f
2 15 0 30
fvasv vvfsv advfd fvfvf vvasf vavfv vsvdv vfdvf fvdvf sdvfv

## Review with v
2 15 0 30
varus vasty lav var; sylva vat guv; davy vulgar ulvas

## From b to j
2 15 0 30
fbasb bbfsb adbfd fbfbf bbasf babfb bsbdb bfdbf fbdbf sdbfb

## review with b
2 15 0 30
bad labs dab bald albs; tabs flab bard jab; grub buys garb

## review 2
2 15 0 30
bad vag bug bar vug vas vat bav bay bat bag bah vast bur

## review 3
2 15 0 30
baby vagary burst vary ruby valuta buy vast vault vulgar by

## From n to j
2 15 0 30
jn;ln nnjln ;knjk jnjnj nn;lj n;njn nlnkn njknj jnknj lknjn

## Review with n
2 15 0 30
nag and fang; dank hang hank nah; flank land sangh khan

## Upper review
2 15 0 30
the rags rang vast; bag lard jant ban yards; stud turk

## From m to j
2 15 0 30
jm;lm mmjlm ;kmjk jmjmj mm;lj m;mjm mlmkm mjkmj jmkmj lkmjm

## Review with m
2 15 0 30
mum mut sham rams slam; slum gama smut balm arm mans flam

## review 1
2 15 0 30
mad marts smart human; numb ban nag ma and mass lamb rut

## review 2
2 15 0 30
vans tan rash fangs; balm must slam sand; arms man salt

## review 3
2 15 0 30
ham gun jam ran gum man fun mat nab arm sun may nut tun mud

## review 4
2 15 0 30
ham gun jam ran gum man fun mat nab arm sun may nut tun mud

## review 5
2 15 0 30
hang damm harm darn farm hung lamb rang sand tang tank many

## review 6
2 15 0 30
must bank gang busy hand thank bury junk human marry funny

## review 7
2 15 0 30
numb bush ray baulk flask bald stuff bask shark navy hurry



#### middle and ring fingers 1

## From d to e
2 15 0 30
dease eedse afedf deded eeasd eaede esefe edfed defed sfede

## Review with e
2 15 0 30
dead ed eats eels; the deal dreads to trend; red age fed

## Left side
2 15 0 30
vane envy ends vets vent; use menu but let ten stay fed

## From s to w
2 15 0 30
swadw wwsdw afwsf swsws wwads wawsw wdwfw wsfws swfws dfwsw

## Review with w
2 15 0 30
sway jaws swat the swan; news was swell when laws saw newt

## From k to i
2 15 0 30
ki;li iikli ;jikj kikik ii;lk i;iki iliji ikjik kijik ljiki

## Review with i
2 15 0 30
kids like kin; kiln kits hike; kale shakes skate unkind

## Right side
2 15 0 30
skew vader with likes; liken jade at envy drew swung

## From l to o
2 15 0 30
lo;ko oolko ;jolj lolol oo;kl o;olo okojo oljol lojol kjolo

## Review with o
2 15 0 30
old lords sold bold lots; lads of lore glob local glue

## review 1
2 15 0 30
glow alone or redo ideas; sweat on the show for one halo

## review 2
2 15 0 30
hid ill oil for hit out rid dog hot old too sit oat fin aim

## review 3
2 15 0 30
red tea way leg tie let see owe wet set lie few how sir who

#### middle and ring fingers 2

## From d to c
2 15 0 30
dcasc ccdsc afcdf dcdcd ccasd cacdc cscfc cdfcd dcfcd sfcdc

## Review with c
2 15 0 30
act coy card aced dice can tacs duct ice ache calm duck

## From s to x
2 15 0 30
sxadx xxsdx afxsf sxsxs xxads xaxsx xdxfx xsfxs sxfxs dfxsx

## Review with x
2 15 0 30
six oxen on axes sew sox; coax exams so taxis sac boxes

## Left side
2 15 0 30
jack of care and cash; cats cite chum such that tacks can

## From k to ,
2 15 0 30
k,;l, ,,kl, ;j,kj k,k,k ,,;lk ,;,k, ,l,j, ,kj,k k,j,k lj,k,

## Review with ,
2 15 0 30
kids, like kin, lax fox, and taxi, stick fix, crux hex

## From l to .
2 15 0 30
l.;k. ..lk. ;j.lj l.l.l ..;kl .;.l. .k.j. .lj.l l.j.l kj.l.

## review 1
2 15 0 30
nil. taxes, ink, and oil... yaks, like oxen, are real.

## review 2
2 15 0 30
why, yes. low, two, was, den. led. ebb, ten, you. new, met.

## review 3
2 15 0 30
act, six icy cub fix cab car. wax cry arc axe, can cat cod

## review 4
2 15 0 30
also take, wake late ease, joke sort food obey under beyond

## review 5
2 15 0 30
after death known; early first jewel large offer raise order

## review 6
2 15 0 30
hotel later ready. agree dirty earth, floor weight water soil

## review 7
2 15 0 30
night knife; house lunch naught yield; world story where habit



#### pinkies

## From a to q
2 15 0 30
aqdsq qqasq dfqaf aqaqa qqdsa qdqaq qsqfq qafqa aqfqa sfqaq

## a and q Practice
2 15 0 30
qi quay aquit and quilts; quill and quad are equal.

## From a to z
2 15 0 30
azdsz zzasz dfzaf azaza zzdsa zdzaz zszfz zafza azfza sfzaz

## Left Pinky Review
2 15 0 30
zag and zig wiz quiz zed and zoe over azure zulu zen

## review 1
2 15 0 30
zoo quit zit quite zoom quick zest zing quake zeal zany

## From ; to p
2 15 0 30
;pklp pp;lp kjp;j ;p;p; ppkl; pkp;p plpjp p;jp; ;pjp; ljp;p

## ; and p Practice
2 15 0 30
pole lops lofty pears; please copy cops, pin proper pig

## From ; to /
2 15 0 30
/p;'l /p'l; p;'/l /;p'l ;p/p; /';/; /p;/; /';/; p;/'/ /lp;'/

## From ; to /
2 15 0 30
;/kl/ //;l/ kj/;j ;/;/; //kl; /k/;/ /l/j/ /;j/; ;/j/; lj/;/

## / review
2 15 0 30
plus/minus; miles/hour; cm/sec; /marquee/ peace/poker

## Right Pinky Review
2 15 0 30
',' ';' '.' '/' // ; // '/' '.' ';' ','

## ' and . review
2 15 0 30
acropolis' quack, zigzag'zippy,  per' poster. zirconium' pearl.

## [ and '
2 10 0 30
;p[]'; ';p[] ;p[]'; ';p[] ;p[]'; ';p[] ;p[]'; ';p[]

## review 1
2 15 0 30
put opaque [pup] puzzle [proposal] [prompt] plan [pomp] lamp

## review 1
2 15 0 30
plus/minus; acropolis' [appall] miles/hour [cm/sec] per' pair

## review 2
2 15 0 30
zombie square; poetic /marquee/ question prize [quiz] proper

## review 3
2 15 0 30
poster. price queen [plate] zippy, reply zero km/h quality;

## review 4
2 15 0 30
zigzag' porosity, quantity peace/poker camp zodiac damp plan

## review 4
2 15 0 30
zone quarter. prosperity zirconium' /pound/ power; [press]

## review 5
2 15 0 30
zipper [zoological] quack, piece proud; pearl. penetrate/


#### repetition 1

## mean mean
2 25 0 40
mean mean mean mean mean mean mean mean mean mean mean

## jeans jeans
2 25 0 40
jeans jeans jeans jeans jeans jeans jeans jeans jeans jeans

## echo echo
2 25 0 40
echo echo echo echo echo echo echo echo echo echo echo echo

## thin thin
2 25 0 40
thin thin thin thin thin thin thin thin thin thin thin thin

## disk disk
2 25 0 40
disk disk disk disk disk disk disk disk disk disk disk disk

## dish dish
2 25 0 40
dish dish dish dish dish dish dish dish dish dish dish dish

## dale dale
2 25 0 40
dale dale dale dale dale dale dale dale dale dale dale dale

## oils oils
2 25 0 40
oils oils oils oils oils oils oils oils oils oils oils oils

## path path
2 25 0 40
path path path path path path path path path path path path

## last last
2 25 0 40
last last last last last last last last last last last last

## land land
2 25 0 40
land land land land land land land land land land land land

## land land
2 25 0 40
pets pets pets pets pets pets pets pets pets pets pets pets


## review 1
2 20 0 40
mean jeans echo thin disk dish dale oils path last land pets

## review 2
2 20 0 40
mean jeans echo thin disk dish dale oils path last land pets

## review 3
2 20 0 40
mean jeans echo thin disk dish dale oils path last land pets

## review 4
2 20 0 40
mean jeans echo thin disk dish dale oils path last land pets

#### repetition 2

## pound pound
2 25 0 40
pound pound pound pound pound pound pound pound pound pound

## pits pits
2 25 0 40
pits pits pits pits pits pits pits pits pits pits pits pits

## ryas ryas
2 25 0 40
ryas ryas ryas ryas ryas ryas ryas ryas ryas ryas ryas ryas ryas

## ryas ryas
2 25 0 40
ebbed ebbed ebbed ebbed ebbed ebbed ebbed ebbed ebbed ebbed

## risk risk
2 25 0 40
risk risk risk risk risk risk risk risk risk risk risk risk

## reeks reeks
2 25 0 40
reeks reeks reeks reeks reeks reeks reeks reeks reeks reeks

## leak leak
2 25 0 40
leak leak leak leak leak leak leak leak leak leak leak leak

## lens lens
2 25 0 40
lens lens lens lens lens lens lens lens lens lens lens lens

## flux flux
2 25 0 40
flux flux flux flux flux flux flux flux flux flux flux flux

## leave leave
2 25 0 40
leave leave leave leave leave leave leave leave leave leave leave leave

## leis leis
2 25 0 40
leis leis leis leis leis leis leis leis leis leis leis leis

## leaf leaf
2 25 0 40
leaf leaf leaf leaf leaf leaf leaf leaf leaf leaf leaf leaf

## review 1
2 20 0 40
pound pits ryas ebbed risk reeks leak lens flux leave leis leaf

## review 2
2 20 0 40
pound pits ryas ebbed risk reeks leak lens flux leave leis leaf

## review 3
2 20 0 40
pound pits ryas ebbed risk reeks leak lens flux leave leis leaf

## review 4
2 20 0 40
pound pits ryas ebbed risk reeks leak lens flux leave leis leaf

## review 5
2 20 0 40
pound pits ryas ebbed risk reeks leak lens flux leave leis leaf

#### repetition 3

## lief lief
2 25 0 40
lief lief lief lief lief lief lief lief lief lief lief lief

## lazy lazy
2 25 0 40
lazy lazy lazy lazy lazy lazy lazy lazy lazy lazy lazy lazy

## keno keno
2 25 0 40
keno keno keno keno keno keno keno keno keno keno keno keno

## quack quack
2 25 0 40
quack quack quack quack quack quack quack quack quack quack quack quack

## knife knife
2 25 0 40
knife knife knife knife knife knife knife knife knife knife knife knife

## jack jack
2 25 0 40
jack jack jack jack jack jack jack jack jack jack jack jack

## chap chap
2 25 0 40
chap chap chap chap chap chap chap chap chap chap chap chap

## sock sock
2 25 0 40
sock sock sock sock sock sock sock sock sock sock sock sock

## keys keys
2 25 0 40
keys keys keys keys keys keys keys keys keys keys keys keys

## obey obey
2 25 0 40
obey obey obey obey obey obey obey obey obey obey obey obey

## men's men's
2 25 0 40
men's men's men's men's men's men's men's men's men's men's men's men's

## caps caps
2 25 0 40
caps caps caps caps caps caps caps caps caps caps caps caps

## review 1
2 20 0 40
lief lazy keno quack knife jack chap sock keys obey men's caps

## review 2
2 20 0 40
lief lazy keno quack knife jack chap sock keys obey men's caps

## review 3
2 20 0 40
lief lazy keno quack knife jack chap sock keys obey men's caps

## review 4
2 20 0 40
lief lazy keno quack knife jack chap sock keys obey men's caps

## review 5
2 20 0 40
lief lazy keno quack knife jack chap sock keys obey men's caps

#### repetition 4

## vile vile
2 25 0 40
vile vile vile vile vile vile vile vile vile vile vile vile

## fine fine
2 25 0 40
fine fine fine fine fine fine fine fine fine fine fine fine

## vent vent
2 25 0 40
vent vent vent vent vent vent vent vent vent vent vent vent

## vale vale
2 25 0 40
vale vale vale vale vale vale vale vale vale vale vale vale

## back back
2 25 0 40
back back back back back back back back back back back back

## bans bans
2 25 0 40
bans bans bans bans bans bans bans bans bans bans bans bans

## bags bags
2 25 0 40
bags bags bags bags bags bags bags bags bags bags bags bags

## tins tins
2 25 0 40
tins tins tins tins tins tins tins tins tins tins tins tins

## gift gift
2 25 0 40
gift gift gift gift gift gift gift gift gift gift gift gift

## grit grit
2 25 0 40
grit grit grit grit grit grit grit grit grit grit grit grit

## herb herb
2 25 0 40
herb herb herb herb herb herb herb herb herb herb herb herb

## hand hand
2 25 0 40
hand hand hand hand hand hand hand hand hand hand hand hand


## review 1
2 20 0 40
vile fine vent vale back bans bags tins gift grit herb hand

## review 2
2 20 0 40
vile fine vent vale back bans bags tins gift grit herb hand

## review 3
2 20 0 40
vile fine vent vale back bans bags tins gift grit herb hand

## review 4
2 20 0 40
vile fine vent vale back bans bags tins gift grit herb hand

## review 5
2 20 0 40
vile fine vent vale back bans bags tins gift grit herb hand


#### repetition 5

## while out
2 25 0 40
while out while out while out while out while out while out

## young ease
2 25 0 40
young ease young ease young ease young ease young ease

## need food
2 25 0 40
need food need food need food need food need food

## think hard
2 25 0 40
think hard think hard think hard think hard think hard

## gain loss
2 25 0 40
gain loss gain loss gain loss gain loss gain loss gain loss

## win lose
2 25 0 40
win lose win lose win lose win lose win lose win lose

## been gone
2 25 0 40
been gone been gone been gone been gone been gone

## link fast
2 25 0 40
link fast link fast link fast link fast link fast

## close shop
2 25 0 40
close shop close shop close shop close shop close shop

## stay late
2 25 0 40
stay late stay late stay late stay late stay late

## likes mike
2 25 0 40
likes mike likes mike likes mike likes mike likes mike

## real veal
2 25 0 40
real veal real veal real veal real veal real veal

## fief thief
2 25 0 40
fief thief fief thief fief thief fief thief fief thief

## strong will
2 25 0 40
strong will strong will strong will strong will strong will


#### repetition 6

## example
2 25 0 40
example example example example example example example

## children
2 25 0 40
children children children children children children

## together
2 25 0 40
together together together together together together

## thought
2 25 0 40
thought thought thought thought thought thought thought

## causation
2 25 0 40
causation causation causation causation causation causation

## sentence
2 25 0 40
sentence sentence sentence sentence sentence sentence

## previous
2 25 0 40
previous previous previous previous previous previous

## sensational
2 25 0 40
sensational sensational sensational sensational sensational

## join; join;
2 25 0 40
join; join; join; join; join; join; join; join; join; join;

## link; link;
2 25 0 40
link; link; link; link; link; link; link; link; link; link;

## faster faster
2 25 0 40
faster faster faster faster faster faster faster faster faster faster

## reads reads
2 25 0 40
reads reads reads reads reads reads reads reads reads reads

## suit suit
2 25 0 40
suit suit suit suit suit suit suit suit suit suit suit

## change change
2 25 0 40
change change change change change change change change

## strong strong
2 25 0 40
strong strong strong strong strong strong strong strong

#### vowel focus

## first mixer
2 15 0 30
ooiye aiiya ieeyo ioiay eauao aaeoy eoeie yuaao uieyo auoiu

## second mixer
2 15 0 30
aouia eyeuy oyoue eyoeo yyeeu ayioy eueae aoaao oeeey eiooy

## third mixer
2 15 0 30
eyaoe eoyio euuei eyiea yyyai oiuae yeyii yaoia yyuoy euyuy

## final mixer
2 15 0 30
iyaii uyioa aiuea uuoai oiooi uaoou oayua eouyy yeioo aueoo

## vowel review 1
2 15 0 30
ouoie uaueo iyaei yoeia eyaie uaoyi oyaey iyoeo iouya eaiyu

## vowel review 2
2 15 0 30
ieyoi auyei oeaui eyaey oyuae eyoei uyieo aeoyi yioae oiyeu

## vowel review 3
2 15 0 30
ieyoi auyei oeaui eyaey oyuae eyoei uyieo aeoyi yioae oiyeu

## vowel review 4
2 15 0 30
oeiau yoeie ieaua ieyei auaeo yoieu aeyoi auioy eaiae uoaie

## vowel review 5
2 15 0 30
oyeao ieauy ioeya aueai oaeya iueie yoeau eioei aeyei iyuoa

#### alphabet tour

## First Mixer
2 15 0 30
bhoas uaxdg omugi ocznd oaojc jovdd msooq oqsox tvqgs qpyod

## Second Mixer
2 15 0 30
tzvfw oncbf fyoio vbrbf phukq wskem qjxoi wnior sjslo ykaqm

## Third Mixer
2 15 0 30
ztvtk gcluv ixzst jjlor hlofl hpvnm caheb ympwy srypp swzas

## Final Mixer
2 15 0 30
cwfzt mnhjx wbuzv ywgzl nuqcu caoch vqisd jbuzi jekva ljwob

## review 1
2 15 0 30
gowof hrocj ayxle rfkqk dugpw cjxln dma]e xjnup skxnz rmokl

## review 2
2 15 0 30
aixle rlnlb dmywg tvprh lumtk ajrmw heomc zlnuk pfpex ndlyv

## review 3
2 15 0 30
nsptn bwitk zopsw vkstg mdibw auvle quvkn smrkx nithd krihx

## review 4
2 15 0 30
odmyl dlekh xutnv cmdiw lwubr hcken amrug ltnxp kwoby cysna

## review 5
2 15 0 30
kruxm aodnw ylq]s kpayc xuspg nzkej iehxf krmxl hsitb dmysk

#### common combinations

## ing ing
2 25 0 40
ing ing ing ing ing ing ing ing ing ing ing

## tion tion
2 25 0 40
tion tion tion tion tion tion tion tion tion tion tion

## ment ment
2 25 0 40
ment ment ment ment ment ment ment ment ment ment ment

## ure ure
2 25 0 40
ure ure ure ure ure ure ure ure ure ure ure

## sion sion
2 25 0 40
sion sion sion sion sion sion sion sion sion sion sion

## ous ous
2 25 0 40
ous ous ous ous ous ous ous ous ous ous ous

## our our
2 25 0 40
our our our our our our our our our our our

## er or
2 25 0 40
er or er or er or er or er or er or er or

## tch tch
2 25 0 40
tch tch tch tch tch tch tch tch tch tch tch

## ck ch
2 25 0 40
ck ch ck ch ck ch ck ch ck ch ck ch ck ch

## ea ea
2 25 0 40
ea ea ea ea ea ea ea ea ea ea ea

## er er
2 25 0 40
er er er er er er er er er er er

## the ht
2 25 0 40
the ht the ht the ht the ht the ht the ht the ht

## ght ght
2 25 0 40
ght ght ght ght ght ght ght ght ght ght ght

## es ly
2 25 0 40
es ly es ly es ly es ly es ly es ly es ly

## ed ed
2 25 0 40
ed ed ed ed ed ed ed ed ed ed ed

## and and
2 25 0 40
and and and and and and and and and and and

## ous ous
2 25 0 40
ous ous ous ous ous ous ous ous ous ous ous

#### numbers and punctuation

## 7 to 8
2 10 0 30
ju7ki8 7ujki8 78uijk ku87ij j7k87j 8k7j88 78jk78 78uijk

## 7 and 8 practice
2 15 0 30
mean7 jeans8 echo7 thin8 disk7 dish8 dale7 oils8 path7 last8

## 5 and 6
2 12 0 30
fr5gt6 5rfgt6 56rtfg gr65tf f5g65f 6g5f66 56fg56 56rtfg

## 5 and 6 practice
2 15 0 30
land5 pets6 pound8 pits7 ryas5 ebbed6 risk5 reeks7 leak8 lens6

## 9 and 0
2 12 0 30
lo9;p0 9ol;p0 90opl; ;o09pl l9;09l 0;9l00 90l;90 90opl;

## 9 and 0 practice
2 15 0 30
flux9 lave0 leis7 leaf0 lief6 lazy9 keno5 quack9 knife8 jack0

## 3 and 4
2 12 0 30
sw3de4 3wsde4 34wesd dw43es s3d43s 4d3s44 34sd34 34wesd

## 3 and 4 practice
2 15 0 30
chap3 sock4 keys3 obey0 men's7 caps4 vile3 fine4 vent6 vale4

## From ; to =
2 12 0 30
p;-=; =-p; p=-=p; -=--p; --==pp; =--=; p=p; =-p;p-=-=;

## = and - practice
2 15 0 30
back= bans- bags3 tins- gift4 grit= herb5 hand= pink7 chin=

## From 1 and 2
2 12 0 30
aq1221qa q1q2a 2q2a2 1a2a1q 12qa12 2112qa aq2112

## 1 and 2 practice
2 15 0 30
cash2 come1 earn= evil2 form- join3 vote1 deck6 fern2 zeal1

## review 1
2 15 0 30
back= bans- bags3 tins- gift4 grit= herb5 hand= pink7 chin=

## review 2
2 12 0 30
only6 pain3 sale3 rank0===king7 5bait5 35deny5 34find 42 minus - 10

## review 3
2 8 0 30
17 minus - 5 equals = 12 8 minus - 3 equals = 5 10 minus - 3 equals = 7

## review 4
2 8 0 30
6 minus - 4 equals = 2 13 minus - 2 equals = 11 19 minus - 4 equals = 15

## review 5
2 8 0 30
8 minus - 5 equals = 3 5 minus - 1 equals = 4 19 minus - 13 equals = 6

## review 6
2 8 0 30
49 minus - 1 equals = 48 12 minus - 3 equals = 9 15 minus - 9 equals = 6

## review 7
2 8 0 30
12 minus - 8 equals = 4 11 minus - 4 equals = 7 19 minus - 13 equals = 6

## review 8
2 8 0 30
12 minus - 3 equals = 9 10 minus - 6 equals = 4 9 minus - 4 equals = 5



#### left shift, right shift

## review 1
2 12 0 30
GHDJY GYDUC Z:CCH HGXCD V:JPR JGKXG JDJEG YLG<N JENGT CJGCY

## review 2
2 12 0 30
GTY<D FGYDH KYNUN RDA:G F<DUJ LPDUY RLCPF GCGGD YAFCJ YGEXR

## review 3
2 12 0 30
RPVDC D<"DV BVXCN TWYPY DNBTG AGLYV RFKBD HYPVL YRPJC G<H:X

## review 4
2 12 0 30
MAGIC RIVER MONEY NOISE PAPER WOMAN EVENT ANGRY COVER METALM

## review 5
2 12 0 30
COMPANY HORSES HAPPY SPECIAL VENTURE CHANGE RETURN KNIGHT

## review 6
2 12 0 30
MagiC RiveR MoneY NoisE PapeR WomaN EvenT AngrY CoveR MetaL

## review 7
2 12 0 30
ComPanY HorSeS HappY SpeCiaL VenTurE ChaNgE RetUrN KniGhT


#### numbers and punctuation 2

## @ and !
2 10 0 20
aq!@@!qa q!q@a @q@a@ !a@a!q !@qa!@ @!@qa aq@!@

## @ and ! review
2 15 0 30
@song !song @!song @song !@song !song @@!song @song!!

## $ and #
2 10 0 20
sw#de$ #wsde$ #$wesd dw$#es s#d$#s $d#s$$ #$sd#$ #$wesd

## $ and # review
2 15 0 30
rain$ rain#$ rain##$ rain# rain$# rain$ rain$# rain$

## % and ^
2 10 0 20
fr%gt^ %rfgt^ %^rtfg gr^%tf f%g^%f ^g%f^^ %^fg%^ %^rtfg

## % and ^ review
2 15 0 45
pale%^ pale^ pale^%^ pale% pale%^ pale^% pale^ pale%

## & and *
2 10 0 20
ju&ki* &ujki* &*uijk ku*&ij j&k*&j *k&j** &*jk&* &*uijk

## & and * review
2 15 0 30
calf& calf*& calf* calf&*& calf& calf**& calf* calf&

## ( and )
2 10 0 30
lo(;p) (ol;p) ()opl; ;o)(pl l(;)(l );(l)) ()l;() ()opl;

## ( and ) review
2 15 0 30
mask ( mask )( mask) mask ( mask )() mask ) mask () mask )(

## + and _
2 10 0 20
p;_+; +_p; p+_+p; _+__p; __++pp; +__+; p+p; +_p;p_+_+;

## + and _ review
2 15 0 30
mine _ mine +_ mine _+_ mine + mine _+ mine + mine _ mine _+

## symbols review 1
2 10 0 30
1 shift !: 2 shift @: 3 shift #: 4 shift $: 5 shift %: 6 shift ^:

## symbols review 2
2 10 0 30
7 shift &: 8 shift *: 9 shift (: 0 shift ): - shift _: = shift +:

## brackets
2 12 0 45
;p{}'; ';p{} ;p{}'; ';p{} ;p{}'; ';p{} ;p{}'; ';p{}

## bracets review
2 14 0 14
{loop} {loop} {loop} {loop} {loop} {loop} {loop} {loop}

## review 1
2 10 0 30
1. Mean, 2. Jeans, 3. Echo? 4. Thin, 5. Disk? 6. Dish, 7. Last? 8. Oils, 9. Path

#### sentences

## one
2 20 0 40
1. If you can dream it you can do it.

## two
2 20 0 40
2. Be the change that you wish to see in the world.

## three
2 20 0 40
3. If I cannot do great things, I can do small things in a great way.

## four
2 20 0 40
4. There are no wrong answers, only learning opportunities.

## five
2 20 0 40
5. It's not about what it is, it's about what it can become.

## six
2 20 0 40
6. Success is not final, failure is not fatal: it is the courage to continue that counts.

## seven
2 20 0 40
7. You miss 100% of the shots you don't take.

## eight
2 20 0 40
8. Never look down on anybody unless you're helping them up.

## nine
2 20 0 40
9. Don't let what you can't do stop you from doing what you can do.

## ten
2 20 0 40
10. The best time to believe in yourself, even more, is when it's not easy.

## eleven
2 20 0 40
11. Being kind is never wasted.

## twelve
2 20 0 40
12. Trust your head, but don't be afraid to follow your heart.

## thirteen
2 20 0 40
13. You are braver than you believe, stronger than you seem, and smarter than you think.

## fourteen
2 20 0 40
14. We all can dance when we find music we love.

## fifteen
2 20 0 40
15. Leave a place better than you found it.


#### paragraphs

## one
3 15 0 30
In class on Monday morning, Teacher Grace gave grade 5 a challenge: collect nails from the environment.

## two
3 15 0 30
Leo wanted to have the most nails. But Asha wanted to have more nails than Leo.

## three
3 15 0 30
By Tuesday morning, Leo only had 20 nails, but Asha already had 30 nails.

## four
3 15 0 30
At lunch on Tuesday afternoon, Leo heard Tom and Chichi making secret plans.

## five
3 15 0 30
Tom wanted to collect nails from The Construction Site, and Chichi wanted to collect nails from the The New School.

## six
3 15 0 30
In class on Wednesday morning, Asha heard Maggie and Mary making secret plans too.

## seven
3 15 0 30
Maggie wanted to buy nails from The Hardware, and Mary wanted her father to bring nails from The Office.

## eight
3 15 0 30
In class on Thursday morning, Asha and Leo both had 60 nails. So they secretly decided to bring another 40 nails on Friday morning to win the challenge!

## nine
3 15 0 30
On Thursday night, Leo followed Tom to The Construction Site, and Asha followed Maggie to The Hardware.

## ten
3 15 0 30
In class on Friday morning, Asha and Leo both had more nails than anyone else. Asha had 100 nails and Leo had 120 nails!

## eleven
3 15 0 30
But that's when things got ugly.

## twelve
3 15 0 30
Teacher Grace said Mary's nails were not from the environment. That's when Chichi said that he saw Leo sneaking around The Construction Site with Tom.

## thirteen
3 15 0 30
Then Leo said he saw Asha leaving The Hardware with Maggie!

## fourteen
3 15 0 30
Teacher Grace decided she had heard enough. Mary and Chichi and Leo and Tom and Asha and Maggie were all disqualified!

## fifteen
3 15 0 30
And that's how Sasha won the challenge with only 7 nails that she collected in The Market.

## hooray
2 20 0 30
Looks like I am typing a bit faster now. Now it is a good time to try the ninja mode!
