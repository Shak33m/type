## v3.0.25
Remove prefix from gitlab pages

## v3.0.24
Disable caps lock in regular lessons

## v3.0.23
Update lessons

## v3.0.22
Update lessons

## v3.0.21
Update lessons

## v3.0.20
Update lessons

## v3.0.19
Update lessons

## v3.0.18
Update lessons

## v3.0.17
Update lessons

## v3.0.16
Update lessons

## v3.0.15
Polish last level

## v3.0.14
Update lessons

## v3.0.12
Update lessons

## v3.0.11
Update test lines

## v3.0.10
Update lessons

## v3.0.9
Update lessons

## v3.0.8
Update lessons

## v3.0.7
Make mistakes quieter

## v3.0.6
Add deploy script

