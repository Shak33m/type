# TL;DR

When all dependencies are available...

```bash
# Starting a development server
./bin/dev

# To update the lessons, change lessons.md, then run:
./bin/gen-lessons
```

Open `http://localhost:5137` in a browser

# Setup

To work on anything:
  1. Have a unix system (tested on Linux and MacOS)
  1. Have an `sh`-like shell
  1. Have dev tools like `git` (See: [`asdf` requirements](https://asdf-vm.com/guide/getting-started.html#_1-install-dependencies))

To work on the react web client:
  1. Install the `node` version from `.tool-versions`
  1. Install the `pnpm` package manager
  1. Install the modules required by `./client/package.json`

To generate lessons:
  1. Install the `ruby` version from `.tool-versions`

## How to install dependencies with `asdf-vm` and `bash`

Here's how you can clone this repo, install `go` and `node` with `asdf` if you're using `bash`:

```bash
# Cloning this repo
git clone https://gitlab.com/techlit-africa/type
cd type

# Installing the asdf version manager
git clone https://github.com/asdf-vm/asdf ~/.asdf

# Configuring bash to use asdf
echo source ~/.asdf/asdf.sh >> ~/.bashrc

# Reloading bash
source ~/.bashrc

# Installing nodejs and ruby plugins for asdf
asdf plugin add nodejs
asdf plugin add ruby

# Installing nodejs and ruby versions
asdf install nodejs $(cat .tool-versions | grep nodejs | cut -d' ' -f2)
asdf install ruby $(cat .tool-versions | grep ruby | cut -d' ' -f2)

# Installing pnpm package manager
npm i -g pnpm

# Installing node modules
pnpm i
```
