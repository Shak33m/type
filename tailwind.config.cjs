module.exports = {
  mode: 'jit',
  darkMode: 'class',
  content: [
    './index.html',
    './src/**/*.{js,ts,jsx,tsx}',
  ],
  separator: '_',
  theme: {
    extend: {},
  },
  plugins: [],
}
